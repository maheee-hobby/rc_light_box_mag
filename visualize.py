import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.patches import FancyArrowPatch
import numpy as np
from mpl_toolkits.mplot3d import proj3d

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')



import serial

def read_parts(ser):
    parts = ser.readline().decode("ascii").split(",")
    parts[0] = float(parts[0])
    parts[1] = float(parts[1])
    parts[2] = float(parts[2])
    return parts
    
with serial.Serial('COM11') as ser:
    while True:
        parts = read_parts(ser)

        ax.clear()
        ax.set_xlim([-1,1])
        ax.set_ylim([-1,1])
        ax.set_zlim([-1,1])

        ax.plot([0, parts[0]], [0, parts[1]], [0, parts[2]])

        ax.plot([parts[0], 0], [1, 1], [-1, -1])
        ax.plot([-1, -1], [parts[1], 0], [-1, -1])
        ax.plot([-1, -1], [1, 1], [parts[2], 0])

        plt.pause(0.1)

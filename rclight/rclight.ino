#include <Wire.h>
#include <WS2812B.h>
#include "QMC5883.h"

WS2812B ws2812b = WS2812B(1);
QMC5883 qmc5883;

void setup()
{
  Serial.begin(115200);

  while (!qmc5883.begin()) {
    Serial.println("Could not find a valid 5883 sensor, check wiring!");
    delay(500);
  }

  Serial.print("5883: isHMC_= ");
  Serial.println(qmc5883.isHMC());
  Serial.print("5883: isQMC_= ");
  Serial.println(qmc5883.isQMC());

  qmc5883.initDefaults();
  qmc5883.resetCalibration(-1442, 2030, -2703, 763, -4352, -512);

  ws2812b.begin();
  ws2812b.show();

  delay(2000);
}

void loop()
{
  //QmcIntVector mag = qmc5883.readRaw();
  QmcFloatVector mag = qmc5883.readCalibrated();
  //QmcFloatVector mag = qmc5883.readVector();

  Serial.print(mag.XAxis);
  Serial.print(",");
  Serial.print(mag.YAxis);
  Serial.print(",");
  Serial.print(mag.ZAxis);
  //Serial.print(",");
  //Serial.print(mag.length);
  Serial.println();

  ws2812b.setPixelColor(0, ws2812b.Color(
                          (mag.XAxis + 1) * 127,
                          (mag.YAxis + 1) * 127,
                          (mag.ZAxis + 1) * 127));
  ws2812b.show();

  delay(200);
}

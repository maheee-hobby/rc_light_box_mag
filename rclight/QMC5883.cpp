#include "Arduino.h"
#include <Wire.h>
#include "QMC5883.h"


bool QMC5883::begin()
{
  int retry;
  retry = 5;

  while (retry--) {
    Wire.begin();
    Wire.beginTransmission(HMC5883L_ADDRESS);
    isHMC_ = (0 == Wire.endTransmission());
    if (isHMC_) {
      break;
    }
    delay(20);
  }

  if (isHMC_) {
    if ((fastRegister8(HMC5883L_REG_IDENT_A) != 0x48)
        || (fastRegister8(HMC5883L_REG_IDENT_B) != 0x34)
        || (fastRegister8(HMC5883L_REG_IDENT_C) != 0x33)) {
      return false;
    }

    setRange(HMC5883L_RANGE_1_3GA);
    setMeasurementMode(HMC5883L_CONTINOUS);
    setDataRate(HMC5883L_DATARATE_15HZ);
    setSamples(HMC5883L_SAMPLES_1);
    mgPerDigit = 0.92f;
    return true;
  } else {
    retry = 5;
    while (retry--) {
      Wire.begin();
      Wire.beginTransmission(QMC5883_ADDRESS);
      isQMC_ = (0 == Wire.endTransmission());
      if (isHMC_) {
        break;
      }
      delay(20);
    }
    if (isQMC_) {
      writeRegister8(QMC5883_REG_IDENT_B, 0X01);
      writeRegister8(QMC5883_REG_IDENT_C, 0X40);
      writeRegister8(QMC5883_REG_IDENT_D, 0X01);
      writeRegister8(QMC5883_REG_CONFIG_1, 0X1D);
      if ((fastRegister8(QMC5883_REG_IDENT_B) != 0x01)
          || (fastRegister8(QMC5883_REG_IDENT_C) != 0x40)
          || (fastRegister8(QMC5883_REG_IDENT_D) != 0x01)) {
        return false;
      }
      setRange(QMC5883_RANGE_8GA);
      setMeasurementMode(QMC5883_CONTINOUS);
      setDataRate(QMC5883_DATARATE_50HZ);
      setSamples(QMC5883_SAMPLES_8);
      mgPerDigit = 4.35f;
      return true;
    }
  }
  return false;
}

void QMC5883::initDefaults()
{
  if (isHMC_) {
    setRange(HMC5883L_RANGE_1_3GA);
    setMeasurementMode(HMC5883L_CONTINOUS);
    setDataRate(HMC5883L_DATARATE_15HZ);
    setSamples(HMC5883L_SAMPLES_8);
  } else if (isQMC_) {
    setRange(QMC5883_RANGE_2GA);
    setMeasurementMode(QMC5883_CONTINOUS);
    setDataRate(QMC5883_DATARATE_50HZ);
    setSamples(QMC5883_SAMPLES_8);
  }
}

QmcIntVector QMC5883::readRaw(void)
{
  if (isHMC_) {
    v.XAxis = readRegister16(HMC5883L_REG_OUT_X_M);
    v.YAxis = readRegister16(HMC5883L_REG_OUT_Y_M);
    v.ZAxis = readRegister16(HMC5883L_REG_OUT_Z_M);
  } else if (isQMC_) {
    v.XAxis = readRegister16(QMC5883_REG_OUT_X_M);
    v.YAxis = readRegister16(QMC5883_REG_OUT_Y_M);
    v.ZAxis = readRegister16(QMC5883_REG_OUT_Z_M);
  } else {
    v.XAxis = 0;
    v.YAxis = 0;
    v.ZAxis = 0;
  }
  return v;
}

QmcFloatVector QMC5883::readCalibrated()
{
  readRaw();
  calibrate();

  double vLength = sqrt((v.XAxis * v.XAxis) + (v.YAxis * v.YAxis) + (v.ZAxis * v.ZAxis));

  vf.XAxis = (v.XAxis - minX) / (float)(maxX - minX);
  vf.YAxis = (v.YAxis - minY) / (float)(maxY - minY);
  vf.ZAxis = (v.ZAxis - minZ) / (float)(maxZ - minZ);

  vf.XAxis -= 0.5;
  vf.YAxis -= 0.5;
  vf.ZAxis -= 0.5;

  vf.XAxis *= 2;
  vf.YAxis *= 2;
  vf.ZAxis *= 2;
  vf.length = vLength;

  return vf;
}

QmcFloatVector QMC5883::readVector()
{
  readRaw();

  double vLength = sqrt((v.XAxis * v.XAxis) + (v.YAxis * v.YAxis) + (v.ZAxis * v.ZAxis));

  vf.XAxis = v.XAxis / vLength;
  vf.YAxis = v.YAxis / vLength;
  vf.ZAxis = v.ZAxis / vLength;

  vf.length = vLength;

  return vf;
}

void QMC5883::resetCalibration()
{
  resetCalibration(
    INT16_MAX, INT16_MIN,
    INT16_MAX, INT16_MIN,
    INT16_MAX, INT16_MIN);
}

void QMC5883::resetCalibration(int16_t minX, int16_t maxX, int16_t minY, int16_t maxY, int16_t minZ, int16_t maxZ)
{
  this->minX = minX;
  this->maxX = maxX;
  this->minY = minY;
  this->maxY = maxY;
  this->minZ = minZ;
  this->maxZ = maxZ;
}

void QMC5883::calibrate()
{
  if (v.XAxis < minX ) minX = v.XAxis;
  if (v.XAxis > maxX ) maxX = v.XAxis;
  if (v.YAxis < minY ) minY = v.YAxis;
  if (v.YAxis > maxY ) maxY = v.YAxis;
  if (v.ZAxis < minZ ) minZ = v.ZAxis;
  if (v.ZAxis > maxZ ) maxZ = v.ZAxis;
}

void QMC5883::setRange(QMC5883_range_t range)
{
  if (isHMC_) {
    switch (range) {
      case HMC5883L_RANGE_0_88GA:
        mgPerDigit = 0.073f;
        break;

      case HMC5883L_RANGE_1_3GA:
        mgPerDigit = 0.92f;
        break;

      case HMC5883L_RANGE_1_9GA:
        mgPerDigit = 1.22f;
        break;

      case HMC5883L_RANGE_2_5GA:
        mgPerDigit = 1.52f;
        break;

      case HMC5883L_RANGE_4GA:
        mgPerDigit = 2.27f;
        break;

      case HMC5883L_RANGE_4_7GA:
        mgPerDigit = 2.56f;
        break;

      case HMC5883L_RANGE_5_6GA:
        mgPerDigit = 3.03f;
        break;

      case HMC5883L_RANGE_8_1GA:
        mgPerDigit = 4.35f;
        break;

      default:
        break;
    }

    writeRegister8(HMC5883L_REG_CONFIG_B, range << 5);
  } else if (isQMC_) {
    switch (range)
    {
      case QMC5883_RANGE_2GA:
        mgPerDigit = 1.22f;
        break;
      case QMC5883_RANGE_8GA:
        mgPerDigit = 4.35f;
        break;
      default:
        break;
    }

    writeRegister8(QMC5883_REG_CONFIG_2, range << 4);
  }
}

QMC5883_range_t QMC5883::getRange(void)
{
  if (isHMC_) {
    return (QMC5883_range_t)((readRegister8(HMC5883L_REG_CONFIG_B) >> 5));
  } else if (isQMC_) {
    return (QMC5883_range_t)((readRegister8(QMC5883_REG_CONFIG_2) >> 4));
  }
  return QMC5883_RANGE_8GA;
}

void QMC5883::setMeasurementMode(QMC5883_mode_t mode)
{
  uint8_t value;
  if (isHMC_) {
    value = readRegister8(HMC5883L_REG_MODE);
    value &= 0b11111100;
    value |= mode;

    writeRegister8(HMC5883L_REG_MODE, value);
  } else if (isQMC_) {
    value = readRegister8(QMC5883_REG_CONFIG_1);
    value &= 0xfc;
    value |= mode;

    writeRegister8(QMC5883_REG_CONFIG_1, value);
  }
}

QMC5883_mode_t QMC5883::getMeasurementMode(void)
{
  uint8_t value = 0;
  if (isHMC_) {
    value = readRegister8(HMC5883L_REG_MODE);
  } else if (isQMC_) {
    value = readRegister8(QMC5883_REG_CONFIG_1);
  }
  value &= 0b00000011;
  return (QMC5883_mode_t)value;
}

void QMC5883::setDataRate(QMC5883_dataRate_t dataRate)
{
  uint8_t value;
  if (isHMC_) {
    value = readRegister8(HMC5883L_REG_CONFIG_A);
    value &= 0b11100011;
    value |= (dataRate << 2);

    writeRegister8(HMC5883L_REG_CONFIG_A, value);
  } else if (isQMC_) {
    value = readRegister8(QMC5883_REG_CONFIG_1);
    value &= 0xf3;
    value |= (dataRate << 2);

    writeRegister8(QMC5883_REG_CONFIG_1, value);
  }
}

QMC5883_dataRate_t QMC5883::getDataRate(void)
{
  uint8_t value = 0;
  if (isHMC_) {
    value = readRegister8(HMC5883L_REG_CONFIG_A);
    value &= 0b00011100;
    value >>= 2;
  } else if (isQMC_) {
    value = readRegister8(QMC5883_REG_CONFIG_1);
    value &= 0b00001100;
    value >>= 2;
  }
  return (QMC5883_dataRate_t)value;
}

void QMC5883::setSamples(QMC5883_samples_t samples)
{
  uint8_t value;
  if (isHMC_) {
    value = readRegister8(HMC5883L_REG_CONFIG_A);
    value &= 0b10011111;
    value |= (samples << 5);
    writeRegister8(HMC5883L_REG_CONFIG_A, value);
  } else if (isQMC_) {
    value = readRegister8(QMC5883_REG_CONFIG_1);
    value &= 0x3f;
    value |= (samples << 6);
    writeRegister8(QMC5883_REG_CONFIG_1, value);
  }
}

QMC5883_samples_t QMC5883::getSamples(void)
{
  uint8_t value = 0;
  if (isHMC_) {
    value = readRegister8(HMC5883L_REG_CONFIG_A);
    value &= 0b01100000;
    value >>= 5;
  } else if (isQMC_) {
    value = readRegister8(QMC5883_REG_CONFIG_1);
    value &= 0x3f;
    value >>= 6;
  }
  return (QMC5883_samples_t)value;
}

// Write byte to register
void QMC5883::writeRegister8(uint8_t reg, uint8_t value)
{
  if (isHMC_) {
    Wire.beginTransmission(HMC5883L_ADDRESS);
    Wire.write(reg);
    Wire.write(value);
    Wire.endTransmission();
  } else if (isQMC_) {
    Wire.beginTransmission(QMC5883_ADDRESS);
    Wire.write(reg);
    Wire.write(value);
    Wire.endTransmission();
  }
}
// Read byte to register
uint8_t QMC5883::fastRegister8(uint8_t reg)
{
  uint8_t value = 0;
  if (isHMC_) {
    Wire.beginTransmission(HMC5883L_ADDRESS);
    Wire.write(reg);
    Wire.endTransmission();

    Wire.requestFrom(HMC5883L_ADDRESS, 1);
    value = Wire.read();
    Wire.endTransmission();
  } else if (isQMC_) {
    Wire.beginTransmission(QMC5883_ADDRESS);
    Wire.write(reg);
    Wire.endTransmission();
    Wire.requestFrom(QMC5883_ADDRESS, 1);
    value = Wire.read();
    Wire.endTransmission();
  }
  return value;
}

// Read byte from register
uint8_t QMC5883::readRegister8(uint8_t reg)
{
  uint8_t value = 0;
  if (isHMC_) {
    Wire.beginTransmission(HMC5883L_ADDRESS);
    Wire.write(reg);
    Wire.endTransmission();

    Wire.beginTransmission(HMC5883L_ADDRESS);
    Wire.requestFrom(HMC5883L_ADDRESS, 1);
    while (!Wire.available()) {};
    value = Wire.read();
    Wire.endTransmission();
  } else if (isQMC_) {
    Wire.beginTransmission(QMC5883_ADDRESS);
    Wire.write(reg);
    Wire.endTransmission();
    Wire.beginTransmission(QMC5883_ADDRESS);
    Wire.requestFrom(QMC5883_ADDRESS, 1);
    while (!Wire.available()) {};
    value = Wire.read();
    Wire.endTransmission();
  }
  return value;
}
// Read word from register
int16_t QMC5883::readRegister16(uint8_t reg)
{
  int16_t value = 0;
  if (isHMC_) {
    Wire.beginTransmission(HMC5883L_ADDRESS);
    Wire.write(reg);
    Wire.endTransmission();
    Wire.beginTransmission(HMC5883L_ADDRESS);
    Wire.requestFrom(HMC5883L_ADDRESS, 2);
    while (!Wire.available()) {};
    uint8_t vha = Wire.read();
    uint8_t vla = Wire.read();
    Wire.endTransmission();
    value = vha << 8 | vla;
  } else if (isQMC_) {
    Wire.beginTransmission(QMC5883_ADDRESS);
    Wire.write(reg);
    Wire.endTransmission();
    Wire.beginTransmission(QMC5883_ADDRESS);
    Wire.requestFrom(QMC5883_ADDRESS, 2);
    while (!Wire.available()) {};
    uint8_t vha = Wire.read();
    uint8_t vla = Wire.read();
    Wire.endTransmission();
    value = vha << 8 | vla;
  }
  return value;
}

int QMC5883::getICType(void)
{
  if (isHMC_) {
    return IC_HMC5883L;
  } else if (isQMC_) {
    return IC_QMC5883;
  } else {
    return IC_NONE;
  }
}
